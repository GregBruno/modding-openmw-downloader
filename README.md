# modding-openmw-downloader

### Purpose
This script is used to scrape mods from the lists provided on https://modding-openmw.com/lists/.

It uses the Nexus API to find & download the mods that are hosted on nexusmods.com. Mods that are not hosted on Nexus Mods will need to be downloaded by hand.

This script writes relative information to a CSV located in the same directory that the script is called from. 
Information such as 'Usage Notes' will be incomplete if there is a line-break in that section on the mod page located at https://modding-openmw.com/mods/{mod_name}/.. sorry about that.

### Pre-Reqs
The following packages must be installed and in your `${PATH}`:
- unrar
- 7z
- unzip
- curl

### Usage

Clone this repo and mark the script as executable. 
Example:
`chmod 700 openmw-downloader.sh`

Export your Nexus API Key as an environment variable called `NEXUS_API_KEY`.
Example:
`export NEXUS_API_KEY="FooBarBaz123"`

Update the `URL` variable in the script with the URL of the list you would like to download from.
Lists can be found at https://modding-openmw.com/lists/ 

Update the `DIRECTORY` variable in the script with the path you would like to download and unarchive/decompress the mods to.

After you have updated the necessary vars and set your Nexus API Key as an env var, simply call the script via `./openmw-downloader.sh`

Once the script finishes, it will output config entries that should be placed in your `openmw.cfg`. 
See https://openmw.readthedocs.io/en/stable/reference/modding/mod-install.html for the necessary steps on adding mods to your config file.
